﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Http.OData;
using ProjFinal.Domain;

namespace ProjFinal.API.Controllers
{
    public class ProdutoMaquinasController : ApiController
    {
        private DataContext db = new DataContext();

        // GET: api/ProdutoMaquinas
        [EnableQuery]
        public IQueryable<ProdutoMaquina> GetProdutoMaquinas()
        {
            return db.ProdutoMaquinas;
        }

        // GET: api/ProdutoMaquinas/5
        [ResponseType(typeof(ProdutoMaquina))]
        public IHttpActionResult GetProdutoMaquina(int id)
        {
            ProdutoMaquina produtoMaquina = db.ProdutoMaquinas.Find(id);
            if (produtoMaquina == null)
            {
                return NotFound();
            }

            return Ok(produtoMaquina);
        }

        // PUT: api/ProdutoMaquinas/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutProdutoMaquina(int id, ProdutoMaquina produtoMaquina)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != produtoMaquina.IdProdutoMaquina)
            {
                return BadRequest();
            }

            db.Entry(produtoMaquina).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ProdutoMaquinaExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/ProdutoMaquinas
        [ResponseType(typeof(ProdutoMaquina))]
        public IHttpActionResult PostProdutoMaquina(ProdutoMaquina produtoMaquina)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.ProdutoMaquinas.Add(produtoMaquina);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = produtoMaquina.IdProdutoMaquina }, produtoMaquina);
        }

        // DELETE: api/ProdutoMaquinas/5
        [ResponseType(typeof(ProdutoMaquina))]
        public IHttpActionResult DeleteProdutoMaquina(int id)
        {
            ProdutoMaquina produtoMaquina = db.ProdutoMaquinas.Find(id);
            if (produtoMaquina == null)
            {
                return NotFound();
            }

            db.ProdutoMaquinas.Remove(produtoMaquina);
            db.SaveChanges();

            return Ok(produtoMaquina);
        }

        /// <summary>
        /// Get by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Route("api/ProdutoMaquinas/GetById/{id}")]
        public IHttpActionResult GetById(int id)
        {
            try
            {
                var productLis = db.ProdutoMaquinas.Where(m => m.IdMaquina == id).ToList();

                if (productLis.Count == 0)
                {
                    return NotFound();
                }
                return Ok(productLis);
            }
            catch
            {
                return BadRequest();
            }
        }


       

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool ProdutoMaquinaExists(int id)
        {
            return db.ProdutoMaquinas.Count(e => e.IdProdutoMaquina == id) > 0;
        }
    }
}