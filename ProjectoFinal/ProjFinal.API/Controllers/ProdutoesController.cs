﻿

namespace ProjFinal.API.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    using System.Linq;
    using System.Net;
    using System.Net.Http;
    using System.Threading.Tasks;
    using System.Web.Http;
    using System.Web.Http.Description;
    using System.Web.Http.OData;
    using ProjFinal.Domain;


    public class ProdutoesController : ApiController
    {
        private DataContext db = new DataContext();
        
        // GET: api/Produtoes
        [EnableQuery]
        public IQueryable<Produto> GetProdutoes()
        {
            return db.Produtoes;
        }

        // GET: api/Produtoes/5
        [ResponseType(typeof(Produto))]
        public async Task<IHttpActionResult> GetProduto(int id)
        {
            Produto produto = await db.Produtoes.FindAsync(id);
            if (produto == null)
            {
                return NotFound();
            }
        
            return Ok(produto);
        }



        [Route("api/Produtos/GetByUser/{user}/")]
        [ResponseType(typeof(Produto))]
        public IHttpActionResult GetByUser(string user)
        {
            var maquina = db.Maquinas.Where(x => x.nomeUser == user).FirstOrDefault();
            if (maquina == null)
            {
                return Ok(new { ErrorMessage = "Não tem produtos" });
            }

            var produtosmaquinas = db.ProdutoMaquinas.Where(x => x.IdMaquina == maquina.IdMaquina && x.Validade >= DateTime.Now).ToList();
            var listaProdutosId = produtosmaquinas.Select(x => x.IdProduto).ToList(); 
            if (produtosmaquinas == null)
            {
                return Ok(new { ErrorMessage = "Não tem produtos" });
            }
            var result = from x in db.Produtoes
            where listaProdutosId.Contains(x.IdProduto)
            select x;
            Random getrandom = new Random();
            if(getrandom.Next(0, 2) == 1)
            {

                Alerta alerta = new Alerta {
                    Mensagem = "A maquina Encravou",
                    Status = StatusAlerta.Ativo,
                    IdMaquina = maquina.IdMaquina,
                    Data = DateTime.Now
                     
               };

                db.Entry(alerta).State = EntityState.Added;

                try
                {
                    db.SaveChanges();
                }
                catch (DbUpdateConcurrencyException e)
                {
                   
                    return Ok(new { ErrorMessage = "Ocorreu um erro com a conexão com a base de dados " + e.Message });
                }
                return Ok(new { ErrorMessage = "A maquina encravou" });

            }

            return Ok(result.ToList());
           

        }



        // PUT: api/Produtoes/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutProduto(int id, Produto produto)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != produto.IdProduto)
            {
                return BadRequest();
            }

            db.Entry(produto).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ProdutoExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Produtoes
        [ResponseType(typeof(Produto))]
        public async Task<IHttpActionResult> PostProduto(Produto produto)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Produtoes.Add(produto);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = produto.IdProduto }, produto);
        }

        // DELETE: api/Produtoes/5
        [ResponseType(typeof(Produto))]
        public async Task<IHttpActionResult> DeleteProduto(int id)
        {
            Produto produto = await db.Produtoes.FindAsync(id);
            if (produto == null)
            {
                return NotFound();
            }

            db.Produtoes.Remove(produto);
            await db.SaveChangesAsync();

            return Ok(produto);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool ProdutoExists(int id)
        {
            return db.Produtoes.Count(e => e.IdProduto == id) > 0;
        }
    }
}