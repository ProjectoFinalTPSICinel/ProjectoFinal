﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Http.OData;
using ProjFinal.Domain;

namespace ProjFinal.API.Controllers
{
    public class FaturasController : ApiController
    {
        private DataContext db = new DataContext();

        // GET: api/Faturas
        [EnableQuery]
        public IQueryable<Fatura> GetFaturas()
        {
            return db.Faturas;
        }

        // GET: api/Faturas/5
        [ResponseType(typeof(Fatura))]
        public IHttpActionResult GetFatura(int id)
        {
            Fatura fatura = db.Faturas.Find(id);
            if (fatura == null)
            {
                return NotFound();
            }

            return Ok(fatura);
        }

        // PUT: api/Faturas/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutFatura(int id, Fatura fatura)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != fatura.IdFatura)
            {
                return BadRequest();
            }

            db.Entry(fatura).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!FaturaExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // Post: api/Faturas/EfectuarCompra/1
        [Route("api/Produtos/PostFaturaComprar/{idProduto}/")]
        [ResponseType(typeof(Produto))]
        public IHttpActionResult PostFaturaComprar(int IdProduto)
        {

            Produto produto = db.Produtoes.Where(x => x.IdProduto == IdProduto).First();
            ProdutoMaquina produtoMaquina = db.ProdutoMaquinas.Where(x => x.IdProduto == IdProduto).FirstOrDefault();

            Fatura fatura = new Fatura
            {
                Data = DateTime.Now,
                Valor = produto.Preco,
                ProdutoMaquina = produtoMaquina,
                IdProdutoMaquina = produtoMaquina.IdProdutoMaquina

            };
            try
            {
                db.Entry(fatura).State = EntityState.Added;
            }
            catch (Exception)
            {

                return Ok(new { ErrorMensage = "Ocorreu um erro durante o registo da factura" });
            }
            try
            {
                db.Entry(produtoMaquina).State = EntityState.Deleted;
            }
            catch (Exception)
            {

                return Ok(new { ErrorMensage = "Ocorreu um erro durante a eliminação do produto da maquina" });
            }
            return Ok(new { ErrorMensage = "Compra efectuada com sucesso"});
        }


        // POST: api/Faturas
        [ResponseType(typeof(Fatura))]
        public IHttpActionResult PostFatura(Fatura fatura)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Faturas.Add(fatura);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = fatura.IdFatura }, fatura);
        }

        // DELETE: api/Faturas/5
        [ResponseType(typeof(Fatura))]
        public IHttpActionResult DeleteFatura(int id)
        {
            Fatura fatura = db.Faturas.Find(id);
            if (fatura == null)
            {
                return NotFound();
            }

            db.Faturas.Remove(fatura);
            db.SaveChanges();

            return Ok(fatura);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool FaturaExists(int id)
        {
            return db.Faturas.Count(e => e.IdFatura == id) > 0;
        }
    }
}