﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using ProjFinal.Domain;

namespace ProjFinal.API.Controllers
{
    public class ModeloMaquinasController : ApiController
    {
        private DataContext db = new DataContext();

        // GET: api/ModeloMaquinas
        public IQueryable<ModeloMaquina> GetModeloMaquinas()
        {
            return db.ModeloMaquinas;
        }

        // GET: api/ModeloMaquinas/5
        [ResponseType(typeof(ModeloMaquina))]
        public IHttpActionResult GetModeloMaquina(int id)
        {
            ModeloMaquina modeloMaquina = db.ModeloMaquinas.Find(id);
            if (modeloMaquina == null)
            {
                return NotFound();
            }

            return Ok(modeloMaquina);
        }

        // PUT: api/ModeloMaquinas/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutModeloMaquina(int id, ModeloMaquina modeloMaquina)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != modeloMaquina.IdModeloMaquina)
            {
                return BadRequest();
            }

            db.Entry(modeloMaquina).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ModeloMaquinaExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/ModeloMaquinas
        [ResponseType(typeof(ModeloMaquina))]
        public IHttpActionResult PostModeloMaquina(ModeloMaquina modeloMaquina)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.ModeloMaquinas.Add(modeloMaquina);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = modeloMaquina.IdModeloMaquina }, modeloMaquina);
        }

        // DELETE: api/ModeloMaquinas/5
        [ResponseType(typeof(ModeloMaquina))]
        public IHttpActionResult DeleteModeloMaquina(int id)
        {
            ModeloMaquina modeloMaquina = db.ModeloMaquinas.Find(id);
            if (modeloMaquina == null)
            {
                return NotFound();
            }

            db.ModeloMaquinas.Remove(modeloMaquina);
            db.SaveChanges();

            return Ok(modeloMaquina);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool ModeloMaquinaExists(int id)
        {
            return db.ModeloMaquinas.Count(e => e.IdModeloMaquina == id) > 0;
        }
    }
}