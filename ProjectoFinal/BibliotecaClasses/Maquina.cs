﻿namespace MVC_ProjFinal.Models
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    public class Maquina
    {


        [Key]
        [Display(Name = "Máquina nº")]
        public int IdMaquina { get; set; }

        [Display(Name = "Modelo")]
        [Required]
        public int IdModeloMaquina { get; set; }


        [Display(Name = "Dinheiro recebido")]
        public double DinheiroRecebido { get; set; }

        [Display(Name = "Trocos na Máquina")]
        public double Trocos { get; set; }

        [DataType(DataType.MultilineText)]
        [Display(Name = "Descrição")]
        public string Descricao { get; set; }

        public int idUser { get; set; }

        public string nomeUser { get; set; }



        //Ligações
        public virtual ModeloMaquina ModeloMaquina { get; set; }

        public IEnumerable<ProdutoMaquina> ProdutoMaquina { get; set; }

        public IEnumerable<Alerta> Alerta { get; set; }
    }
}