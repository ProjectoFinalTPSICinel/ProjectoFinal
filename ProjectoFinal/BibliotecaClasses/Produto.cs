﻿namespace MVC_ProjFinal.Models
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    public class Produto
    {
        [Key]
        public int IdProduto { get; set; }

        [Display(Name = "Nome do Produto")]
        public string Nome { get; set; }


        [DataType(DataType.MultilineText)]
        [Display(Name = "Descrição do Produto")]
        public string Descricao { get; set; }

        [Display(Name = "Preço do Produto")]
        public double Preco { get; set; }

        [Display(Name = "Categoria de Produto")]
        public CategoriaProduto CategoriaProduto { get; set; }



        public virtual IEnumerable<ProdutoMaquina> ProdutoMaquina { get; set; }






    }
}