﻿namespace MVC_ProjFinal.Models
{
    public enum CategoriaProduto
    {
        Bebidas,
        Sobremesas,
        Sandes,
        Sopas,
        Pastelaria,
        Chocolates,
        Outros

    }
}