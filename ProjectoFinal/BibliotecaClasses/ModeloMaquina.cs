﻿namespace MVC_ProjFinal.Models
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    public class ModeloMaquina
    {



        [Key]
        public int IdModeloMaquina { get; set; }

        public string Marca { get; set; }

        public string Modelo { get; set; }

        public int CapacidadeProdutos { get; set; }

        public int CapacidadeDinheiro { get; set; }

        public int CapacidadeTrocos { get; set; }

        public TipoMaquina TipoMaquina { get; set; }



        public IEnumerable<Maquina> Maquina { get; set; }

    }
}