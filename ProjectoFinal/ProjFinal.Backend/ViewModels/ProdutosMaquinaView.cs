﻿

namespace ProjFinal.Backend.ViewModels
{
    using ProjFinal.Domain;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    public class ProdutosMaquinaView
    {
        [Display(Name = "Ref. do Produto")]
        public int IdProduto { get; set; }

        [Display(Name = "Nome do Produto")]
        public string NomeProduto { get; set; }

        public int Quantidade { get; set; }

        [DisplayFormat(DataFormatString = "{0:C2}", ApplyFormatInEditMode = false)]
        [DataType(DataType.Currency)]
        [Display(Name = "Preço do Produto")]
        public double Preco { get; set; }


        public ProdutoMaquina ProdutoMaquina { get; set; }


        public IEnumerable<ProdutoMaquina> ProdutosMaquina { get; set; }
    }
}