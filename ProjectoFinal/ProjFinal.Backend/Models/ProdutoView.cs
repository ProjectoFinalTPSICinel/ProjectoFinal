﻿

namespace ProjFinal.Backend.Models
{

    using ProjFinal.Domain;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.ComponentModel.DataAnnotations;
    using System.Web;

    [NotMapped]
    public class ProdutoView:Produto
    {
        [Display(Name ="Image")]
        public HttpPostedFileBase ImageFile { get; set; }

    }

}