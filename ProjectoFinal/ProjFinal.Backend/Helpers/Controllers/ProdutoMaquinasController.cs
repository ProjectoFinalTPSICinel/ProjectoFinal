﻿namespace ProjFinal.Backend.Controllers
{
    using System.Collections.Generic;
    using System.Data;
    using System.Data.Entity;
    using System.Linq;
    using System.Threading.Tasks;
    using System.Net;
    using System.Web.Mvc;
    using ProjFinal.Backend.Models;
    using ProjFinal.Domain;
    using ProjFinal.Backend.ViewModels;
    using System;

    public class ProdutoMaquinasController : Controller
    {
        private DataContextLocal db = new DataContextLocal();

        // GET: TodosProdutosNasMaquinas
        public async Task<ActionResult> TodosProdutosNasMaquinas()
        {

            //produtos em todas as máquinas
            var todosProdutosMaquinas = db.ProdutoMaquinas.Include(p => p.Maquina)
                .Include(p => p.Produto).OrderBy(p=>p.Validade);


            ViewBag.Todos = todosProdutosMaquinas.Count();

            return View(await todosProdutosMaquinas.ToListAsync());

        }

        // GET: ProdutoMaquinas/TodosProdutosNestaMaquina
        public async Task<ActionResult> TodosProdutosNestaMaquina(int IdMaquina)
        {

            //produtos em todas as máquinas
            var produtoMaquinas = db.ProdutoMaquinas.Where(p => p.IdMaquina==IdMaquina).Include(p => p.Maquina)
                .Include(p => p.Produto);

            ViewBag.IdMaquina = IdMaquina;
            return View(await produtoMaquinas.ToListAsync());

        }

        // GET: ProdutoMaquinas/ProdutosNaMaquina
        public async Task<ActionResult> ProdutosAgrupadosNaMaquina(int? idmaquina)
        {

            //se for indicada maquina
            if (idmaquina == null)
            {

                //produtos em todas as máquinas
                var produtoMaquinas = db.ProdutoMaquinas.Include(p => p.Maquina)
                    .Include(p => p.Produto).OrderBy(p => p.IdMaquina);


                return View(await produtoMaquinas.ToListAsync());
            }

            //Produtos na maquina indicada
            List<ProdutoMaquina> produtosMaquina = db.ProdutoMaquinas.Where(p => p.IdMaquina == idmaquina)
                 .Include(p => p.Maquina).Include(p => p.Produto).ToList();
            


            //Agrupar por produto
            var model = from p in produtosMaquina
                        group p by new { p.IdProduto, p.Produto.Nome, p.Produto.Preco } into g
                        select new ProdutosMaquinaView
                        {
                            IdProduto = g.Key.IdProduto,
                            NomeProduto = g.Key.Nome,
                            Preco = g.Key.Preco,
                            Quantidade = g.Count(),
                            ProdutosMaquina = g.ToList(),
                            ProdutoMaquina = new ProdutoMaquina()
                        };


            ViewBag.IdMaquina = idmaquina;

            return View(model);
        }




        // GET: ProdutoMaquinas/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ProdutoMaquina produtoMaquina = await db.ProdutoMaquinas.FindAsync(id);
            if (produtoMaquina == null)
            {
                return HttpNotFound();
            }
            return View(produtoMaquina);
        }


        // GET: ProdutoMaquinas/InserirProdutosNaMaquina
        public ActionResult InserirProdutosNaMaquina(int? idmaquina)
        {

            if (idmaquina == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }


            ViewBag.IdMaquina = idmaquina;
            ViewBag.IdProduto = new SelectList(db.Produtoes, "IdProduto", "Nome");
            return View();
        }

        // POST: ProdutoMaquinas/InserirProdutosNaMaquina
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> InserirProdutosNaMaquina(InserirProdutosViewModel inserirProdutoViewModel)
        {

            //Por cada unidade indicada na quantidade
            for (int i = 1; i <= inserirProdutoViewModel.Quantidade; i++)
            {
                //criar um produto maquina com os parametros indicados
                ProdutoMaquina produtoMaquina = new ProdutoMaquina
                {
                    IdMaquina = inserirProdutoViewModel.IdMaquina,
                    IdProduto = inserirProdutoViewModel.IdProduto,
                    Validade = inserirProdutoViewModel.Validade
                };

                db.ProdutoMaquinas.Add(produtoMaquina);
            }

                await db.SaveChangesAsync();

                ViewBag.IdMaquina = inserirProdutoViewModel.IdMaquina;
                return RedirectToAction("ProdutosAgrupadosNaMaquina", new { idmaquina = inserirProdutoViewModel.IdMaquina });
            

            //ViewBag.IdMaquina = inserirProdutoViewModel.IdMaquina;
            //ViewBag.Error = "Produtos não adicionados!";
            //return RedirectToAction("ProdutosAgrupadosNaMaquina", "ProdutoMaquinas", new { idmaquina = inserirProdutoViewModel.IdMaquina });
        }



        // GET: TodosProdutosNasMaquinas
        public async Task<ActionResult> ProdutosARetirar(int? idMaquina)
        {

            if (idMaquina == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            //produtos na maquina individualmente
            var produtoMaquinas = db.ProdutoMaquinas.Where(p => p.IdMaquina == idMaquina)
                .OrderBy(p => p.IdProdutoMaquina).OrderBy(p => p.Validade);




            ViewBag.IdMaquina = idMaquina;
            return View(await produtoMaquinas.ToListAsync());

        }



        // GET: ProdutoMaquinas/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ProdutoMaquina produtoMaquina = await db.ProdutoMaquinas.FindAsync(id);
            if (produtoMaquina == null)
            {
                return HttpNotFound();
            }
            ViewBag.IdMaquina = new SelectList(db.Maquinas, "IdMaquina", "Descricao", produtoMaquina.IdMaquina);
            ViewBag.IdProduto = new SelectList(db.Produtoes, "IdProduto", "Nome", produtoMaquina.IdProduto);
            return View(produtoMaquina);
        }

        // POST: ProdutoMaquinas/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "IdProdutoMaquina,IdMaquina,IdProduto,Validade")] ProdutoMaquina produtoMaquina)
        {
            if (ModelState.IsValid)
            {
                db.Entry(produtoMaquina).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewBag.IdMaquina = new SelectList(db.Maquinas, "IdMaquina", "Descricao", produtoMaquina.IdMaquina);
            ViewBag.IdProduto = new SelectList(db.Produtoes, "IdProduto", "Nome", produtoMaquina.IdProduto);
            return View(produtoMaquina);
        }

        // GET: ProdutoMaquinas/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ProdutoMaquina produtoMaquina = await db.ProdutoMaquinas.FindAsync(id);
            if (produtoMaquina == null)
            {
                return HttpNotFound();
            }


            return View(produtoMaquina);
        }

        // POST: ProdutoMaquinas/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            ProdutoMaquina produtoMaquina = await db.ProdutoMaquinas.FindAsync(id);
            db.ProdutoMaquinas.Remove(produtoMaquina);
            await db.SaveChangesAsync();

            return RedirectToAction("ProdutosNaMaquina",
                new { idmaquina = produtoMaquina.IdMaquina });
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
