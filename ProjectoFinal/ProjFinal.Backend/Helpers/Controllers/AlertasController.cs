﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ProjFinal.Backend.Models;
using ProjFinal.Domain;

namespace ProjFinal.Backend.Controllers
{
    public class AlertasController : Controller
    {
        private DataContextLocal db = new DataContextLocal();



        // GET: Alertas
        public ActionResult Index(int? idmaquina, string mensagem)
        {

            var alertas = db.Alertas.Include(a => a.Maquina).OrderBy(a => a.Status).OrderByDescending(a => a.Data);

            if (idmaquina != null)
            {
                alertas = alertas.Where(a => a.IdMaquina == idmaquina).OrderBy(a => a.Status).OrderBy(a => a.Data);
            }

            ViewBag.Resultado = mensagem;
            return View(alertas.ToList());
        }

        // GET: Alertas/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Alerta alerta = db.Alertas.Find(id);
            if (alerta == null)
            {
                return HttpNotFound();
            }
            return View(alerta);
        }

        // GET: Alertas/Create
        public ActionResult Create()
        {
            ViewBag.IdMaquina = new SelectList(db.Maquinas, "IdMaquina", "Descricao");
            return View();
        }

        // POST: Alertas/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "IdAlerta,IdMaquina,Mensagem,Status")] Alerta alerta)
        {
            if (ModelState.IsValid)
            {
                db.Alertas.Add(alerta);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.IdMaquina = new SelectList(db.Maquinas, "IdMaquina", "Descricao", alerta.IdMaquina);
            return View(alerta);
        }

        [HttpPost]
        // GET: Alertas/EditVariosAlertas
        public ActionResult EditVariosAlertas(IEnumerable<int> IdsAlertas, int? IdMaquina)
        {
            var alertas = db.Alertas.Where(a => IdsAlertas.Contains(a.IdAlerta)).ToList();

            foreach (Alerta a in alertas)
            {
                a.Status = StatusAlerta.Inativo;

                db.Entry(a).State = EntityState.Modified;
            }


            db.SaveChanges();

            return RedirectToAction("Index", new { idMaquina = IdMaquina });
        }



        //// GET: Alertas/Edit/5
        //public ActionResult Edit(int? id)
        //{
        //    if (id == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }
        //    Alerta alerta = db.Alertas.Find(id);
        //    if (alerta == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    ViewBag.IdMaquina = new SelectList(db.Maquinas, "IdMaquina", "Descricao", alerta.IdMaquina);
        //    return View(alerta);
        //}

        //// POST: Alertas/Edit/5
        //// To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        //// more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public ActionResult Edit([Bind(Include = "IdAlerta,IdMaquina,Mensagem,Status")] Alerta alerta)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        db.Entry(alerta).State = EntityState.Modified;
        //        db.SaveChanges();
        //        return RedirectToAction("Index");
        //    }
        //    ViewBag.IdMaquina = new SelectList(db.Maquinas, "IdMaquina", "Descricao", alerta.IdMaquina);
        //    return View(alerta);
        //}

        // GET: Alertas/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Alerta alerta = db.Alertas.Find(id);
            if (alerta == null)
            {
                return HttpNotFound();
            }
            return View(alerta);
        }

        // POST: Alertas/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Alerta alerta = db.Alertas.Find(id);
            db.Alertas.Remove(alerta);
            db.SaveChanges();
            return RedirectToAction("Index");
        }



        // GET: Alertas/VerificarPrazosDeValidade
        public ActionResult VerificarPrazosDeValidade()
        {
            var produtosForadaValidade = db.ProdutoMaquinas.Where(p => p.Validade < DateTime.Today).ToList();

            string resultado = "";

            try
            {
                foreach (ProdutoMaquina p in produtosForadaValidade)
                {
                    Alerta alerta = new Alerta
                    {
                        IdMaquina = p.IdMaquina,
                        Mensagem = p.Produto.Nome + " fora da Validade na Máquina Nº " + p.IdMaquina,
                        Status = StatusAlerta.Ativo,
                        Data = DateTime.Now
                    };

                    db.Alertas.Add(alerta);


                }

                db.SaveChanges();

                resultado = "Verificacao completa! " + produtosForadaValidade.Count() + " produtos fora da validade!! (" + DateTime.Now + ")";
            }

            catch
            {
                resultado = "A verificacao Incompleta!!";

            }

            return RedirectToAction("Index", new { mensagem = resultado });
        }


        // GET: Alertas/VerificarStocks
        public ActionResult VerificarStocks()
        {


            string resultado = "";
            int count = 0;
            var lista = new List<Stock>();

            foreach (Stock s in db.Stocks)
            {
                lista.Add(s);

            }

            foreach (Stock s in lista)
            {



                var produtos = db.ProdutoMaquinas.Where(p => p.IdMaquina == s.IdMaquina && p.IdProduto == s.IdProduto);


                if (produtos.Count() == 0)
                {
                    Alerta alerta = new Alerta
                    {
                        IdMaquina = s.IdMaquina,
                        Mensagem = s.Produto.Nome + " SEM STOCK na Máquina Nº " + s.IdMaquina,
                        Status = StatusAlerta.Ativo,
                        Data = DateTime.Now,
                    };

                    db.Alertas.Add(alerta);

                    count++;
                }

            }


            db.SaveChanges();



            resultado = "Verificacao completa! " + count + " produtos com falta de Stock!! (" + DateTime.Now + ")";



            //catch
            //{
            //    resultado = "Verificacao Incompleta! (" + DateTime.Now + ")";
            //}

            return RedirectToAction("Index", new
            {
                mensagem = resultado
            });



        }


        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }








    }
}
