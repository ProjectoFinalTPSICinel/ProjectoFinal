﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ProjFinal.Backend.Models;
using ProjFinal.Domain;

namespace ProjFinal.Backend.Controllers
{
    public class FaturasController : Controller
    {
        private DataContextLocal db = new DataContextLocal();

        // GET: Faturas
        public ActionResult Index()
        {
            var faturas = db.Faturas.Include(f => f.ProdutoMaquina);
            return View(faturas.ToList());
        }

        // GET: Faturas/EscolherMaquina
        public ActionResult EscolherMaquina()
        {
            var maquinas = db.Maquinas.Include(m => m.ModeloMaquina);
            return View(maquinas.ToList());
        }

        // GET: Faturas/ProdutosNaMaquina
        public ActionResult ProdutosNaMaquina(int idMaquina)
        {
            //produtos na maquina escolhida
            var produtosMaquina = db.ProdutoMaquinas.Where(p => p.IdMaquina == idMaquina).Include(p=>p.Produto);

            var produtos = produtosMaquina.Select(p => p.Produto);


            ViewBag.IdMaquina = idMaquina;
            return View(produtos);
        }


        //// GET: Faturas/ProdutosNaMaquina
        //public ActionResult ProdutosNaMaquina(int idMaquina)
        //{
        //    //produtos na maquina escolhida
        //    var produtosMaquina = db.ProdutoMaquinas.Where(p => p.IdMaquina == idMaquina)
        //        .OrderBy(p => p.IdProduto).Include(p => p.Maquina)
        //        .Include(p => p.Produto);



        //    return View(produtosMaquina);
        //}


        // GET: Faturas/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Fatura fatura = db.Faturas.Find(id);
            if (fatura == null)
            {
                return HttpNotFound();
            }
            return View(fatura);
        }

        // GET: Faturas/Create
        public ActionResult Create()
        {
            ViewBag.IdProdutoMaquina = new SelectList(db.ProdutoMaquinas, "IdProdutoMaquina", "IdProdutoMaquina");
            return View();
        }

        // POST: Faturas/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "IdFatura,IdProdutoMaquina,Valor,Data")] Fatura fatura)
        {
            if (ModelState.IsValid)
            {
                db.Faturas.Add(fatura);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.IdProdutoMaquina = new SelectList(db.ProdutoMaquinas, "IdProdutoMaquina", "IdProdutoMaquina", fatura.IdProdutoMaquina);
            return View(fatura);
        }

        // GET: Faturas/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Fatura fatura = db.Faturas.Find(id);
            if (fatura == null)
            {
                return HttpNotFound();
            }
            ViewBag.IdProdutoMaquina = new SelectList(db.ProdutoMaquinas, "IdProdutoMaquina", "IdProdutoMaquina", fatura.IdProdutoMaquina);
            return View(fatura);
        }

        // POST: Faturas/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "IdFatura,IdProdutoMaquina,Valor,Data")] Fatura fatura)
        {
            if (ModelState.IsValid)
            {
                db.Entry(fatura).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.IdProdutoMaquina = new SelectList(db.ProdutoMaquinas, "IdProdutoMaquina", "IdProdutoMaquina", fatura.IdProdutoMaquina);
            return View(fatura);
        }

        // GET: Faturas/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Fatura fatura = db.Faturas.Find(id);
            if (fatura == null)
            {
                return HttpNotFound();
            }
            return View(fatura);
        }


        // POST: Faturas/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Fatura fatura = db.Faturas.Find(id);
            db.Faturas.Remove(fatura);
            db.SaveChanges();
            return RedirectToAction("Index");
        }


        // GET: Faturas/Comprar/5
        public ActionResult Comprar(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            

            Produto produto = db.Produtoes.Find(id);
            if (produto== null)
            {
                return HttpNotFound();
            }
            return View(produto);
        }


        // POST: Faturas/Comprar/5
        [HttpPost, ActionName("Comprar")]
        [ValidateAntiForgeryToken]
        public ActionResult ComprarConfirmed(int id)
        {
            //var produtos = db.ProdutoMaquinas.Where(p => p.IdProduto == id);

            //var idproduto = produtos.Select(p => p.IdProduto);

            var produtosMaquina = db.ProdutoMaquinas.Where(p => p.IdProduto.Equals(id));
            //O que tem a data de validade mais baixa
            ProdutoMaquina produtoARemover = produtosMaquina.OrderBy(a => a.Validade).First();

            db.ProdutoMaquinas.Remove(produtoARemover);
            db.SaveChanges();




            Produto produto = db.Produtoes.Find(produtoARemover.IdProduto);

            Maquina maquina = db.Maquinas.Find(produtoARemover.IdMaquina);

            //Adiciona dinheiro recebido pela comprar
            maquina.DinheiroRecebido = +produto.Preco;
            db.Entry(maquina).State = EntityState.Modified;
            db.SaveChanges();

            //!Não funciona
            //Emissão de fatura/registo de venda
            Fatura fatura = new Fatura
            {
                IdFatura = 0,
                IdProdutoMaquina = produtoARemover.IdProdutoMaquina,
                Valor = produtoARemover.Produto.Preco,
                Data = DateTime.Now.Date
            };
            db.Faturas.Add(fatura);
            db.SaveChanges();


            return RedirectToAction("ProdutosNaMaquina",
                new { idmaquina = produtoARemover.IdMaquina });
        }


        



        // GET: Faturas/Comprar/5
        public ActionResult Comprar2(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ProdutoMaquina produtoMaquina = db.ProdutoMaquinas.Find(id);
            if (produtoMaquina == null)
            {
                return HttpNotFound();
            }
            return View(produtoMaquina);
        }


        // POST: Faturas/Comprar/5
        [HttpPost, ActionName("Comprar2")]
        [ValidateAntiForgeryToken]
        public ActionResult ComprarConfirmed2(int id)
        {

            ProdutoMaquina produtoMaquina = db.ProdutoMaquinas.Find(id);
            db.ProdutoMaquinas.Remove(produtoMaquina);
            db.SaveChanges();


           

            Produto produto = db.Produtoes.Find(produtoMaquina.IdProduto);

            Maquina maquina = new Maquina();

            //Adiciona dinheiro recebido pela comprar
            maquina.DinheiroRecebido = +produto.Preco;
            db.Entry(maquina).State = EntityState.Modified;
            db.SaveChanges();


            //Emissão de fatura/registo de venda
            Fatura fatura = new Fatura
            {
                IdProdutoMaquina = produtoMaquina.IdProdutoMaquina,
                Valor = produtoMaquina.Produto.Preco,
                Data = DateTime.Now.Date
            };
            db.Faturas.Add(fatura);
            db.SaveChanges();
            

            return RedirectToAction("ProdutosNaMaquina",
                new { idmaquina = produtoMaquina.IdMaquina });
        }




        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
