﻿

namespace ProjFinal.Backend.Controllers
{

    using ProjFinal.Backend.Models;
    using ProjFinal.Domain;
    using System.Data.Entity;
    using System.Linq;
    using System.Net;
    using System.Web.Mvc;

    public class ModeloMaquinasController : Controller
    {
        private DataContextLocal db = new DataContextLocal();

        // GET: ModeloMaquinas
        public ActionResult Index()
        {
            return View(db.ModeloMaquinas.ToList());
        }

        // GET: ModeloMaquinas/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ModeloMaquina modeloMaquina = db.ModeloMaquinas.Find(id);
            if (modeloMaquina == null)
            {
                return HttpNotFound();
            }
            return View(modeloMaquina);
        }

        // GET: ModeloMaquinas/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: ModeloMaquinas/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "IdModeloMaquina,Marca,Modelo,CapacidadeProdutos,CapacidadeDinheiro,CapacidadeTrocos,TipoMaquina")] ModeloMaquina modeloMaquina)
        {
            if (ModelState.IsValid)
            {
                db.ModeloMaquinas.Add(modeloMaquina);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(modeloMaquina);
        }

        // GET: ModeloMaquinas/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ModeloMaquina modeloMaquina = db.ModeloMaquinas.Find(id);
            if (modeloMaquina == null)
            {
                return HttpNotFound();
            }
            return View(modeloMaquina);
        }

        // POST: ModeloMaquinas/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "IdModeloMaquina,Marca,Modelo,CapacidadeProdutos,CapacidadeDinheiro,CapacidadeTrocos,TipoMaquina")] ModeloMaquina modeloMaquina)
        {
            if (ModelState.IsValid)
            {
                db.Entry(modeloMaquina).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(modeloMaquina);
        }

        // GET: ModeloMaquinas/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ModeloMaquina modeloMaquina = db.ModeloMaquinas.Find(id);
            if (modeloMaquina == null)
            {
                return HttpNotFound();
            }
            return View(modeloMaquina);
        }

        // POST: ModeloMaquinas/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            ModeloMaquina modeloMaquina = db.ModeloMaquinas.Find(id);
            db.ModeloMaquinas.Remove(modeloMaquina);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
