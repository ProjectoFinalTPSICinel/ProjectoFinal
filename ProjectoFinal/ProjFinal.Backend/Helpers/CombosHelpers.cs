﻿
namespace ProjFinal.Backend.Helpers
{

    using Microsoft.AspNet.Identity;
    using Microsoft.AspNet.Identity.EntityFramework;
    using ProjFinal.Backend.Helpers;
    using ProjFinal.Backend.Models;
    using ProjFinal.Backend.ViewModels;
    using System.Collections.Generic;
    using System.Linq;
    using System.Net;
    using System.Web.Mvc;

    public class CombosHelpers
    {


        private static ApplicationDbContext db = new ApplicationDbContext();


       

        //Lista de Roles
        public static List<IdentityRole> GetRoles()
        {
            var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(db));
            var list = roleManager.Roles.ToList();
            list.Add(new IdentityRole { Id = "", Name = "[Selecione uma permissão...]" });


            return list.OrderBy(r => r.Name).ToList();
        }


        //Lista de Users
        public static List<EmailView> GetUsers()
        {
            var list = new List<EmailView>();
            int i = 1;

            foreach(ApplicationUser a in db.Users)
            {
                list.Add(new EmailView { IdEmail = i, UserEmail = a.UserName });
            }

            
            list.Add(new EmailView { IdEmail = i, UserEmail = "[Selecione um User...]" });

            return list.OrderBy(u => u.UserEmail).ToList();
        }



        ////Lista de Users
        //public static List<ApplicationUser> GetUsers()
        //{
        //    var list = db.Users.ToList();

        //    list.Add(new ApplicationUser { Id = "", UserName = "[Selecione um User...]" });

        //    return list.OrderBy(r => r.UserName).ToList();
        //}

        ////Lista de Users
        //public static List<string> GetUsers()
        //{
        //    var list = db.Users.Select(u=>u.UserName).ToList();

        //    list.Add("[Selecione um User...]");

        //    return list.ToList();
        //}

    }
}