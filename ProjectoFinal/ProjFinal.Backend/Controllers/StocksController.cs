﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ProjFinal.Backend.Models;
using ProjFinal.Domain;

namespace ProjFinal.Backend.Controllers
{
    public class StocksController : Controller
    {
        private DataContextLocal db = new DataContextLocal();

        [Authorize(Roles = "Admin")]
        // GET: Stocks
        public ActionResult Index(int? IdMaquina)
        {
            var stocks = new List<Stock>();

            if (IdMaquina != null)
            {
                stocks = db.Stocks.Where(s => s.IdMaquina == IdMaquina).Include(s => s.Maquina).Include(s => s.Produto).ToList();

                ViewBag.Maquina = IdMaquina;
            }
            else
            {
                stocks = db.Stocks.Include(s => s.Maquina).Include(s => s.Produto).ToList();

            }


            return View(stocks);
        }

        [Authorize(Roles = "Admin")]
        // GET: Stocks/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Stock stock = db.Stocks.Find(id);
            if (stock == null)
            {
                return HttpNotFound();
            }
            return View(stock);
        }

        [Authorize(Roles = "Admin")]
        // GET: Stocks/Create
        public ActionResult Create(int? IdMaquina)
        {
            ViewBag.Maquina = IdMaquina;
            ViewBag.IdMaquina = new SelectList(db.Maquinas, "IdMaquina", "IdMaquina");
            ViewBag.IdProduto = new SelectList(db.Produtoes, "IdProduto", "Nome");
            return View();
        }

        // POST: Stocks/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "IdStock,IdMaquina,IdProduto,StockSeguranca")] Stock stock, int? idMaquina)
        {
            var verificaSeExiste = db.Stocks.FirstOrDefault(s => s.IdMaquina == stock.IdMaquina && s.IdProduto == stock.IdProduto);

            if (verificaSeExiste == null)
            {                
                if (ModelState.IsValid)
                {
                    db.Stocks.Add(stock);
                    db.SaveChanges();

                    return RedirectToAction("Index", new { IdMaquina = idMaquina });
                }
            }

            ViewBag.Error = "Produto já está a ser monitorizado!!";
            ViewBag.Maquina = idMaquina;
            ViewBag.IdMaquina = new SelectList(db.Maquinas, "IdMaquina", "IdMaquina");
            ViewBag.IdProduto = new SelectList(db.Produtoes, "IdProduto", "Nome", stock.IdProduto);
            return View(stock);
        }



        [Authorize(Roles = "Admin")]
        // GET: Stocks/Delete/5
        public ActionResult Delete(int? id, int? IdMaquina)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Stock stock = db.Stocks.Find(id);
            if (stock == null)
            {
                return HttpNotFound();
            }


            ViewBag.IdMaquina = IdMaquina;
            return View(stock);
        }

        // POST: Stocks/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id, int? idMaquina)
        {
            Stock stock = db.Stocks.Find(id);
            db.Stocks.Remove(stock);
            db.SaveChanges();
            return RedirectToAction("Index", new { IdMaquina = idMaquina });
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
