﻿

namespace ProjFinal.Backend.Controllers
{
    using ProjFinal.Backend.Models;
    using ProjFinal.Domain;
    using System;
    using System.Linq;
    using System.Web.Mvc;


    public class HomeController : Controller
    {
        private DataContextLocal db = new DataContextLocal();

        public ActionResult Index()
        {
            

            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

                

    }
}