﻿

namespace ProjFinal.Backend.Controllers
{
    using System.Data;
    using System.Data.Entity;
    using System.Linq;
    using System.Net;
    using System.Web.Mvc;
    using ProjFinal.Backend.Models;
    using ProjFinal.Domain;
    using ProjFinal.Backend.Helpers;
    using System.Collections.Generic;

    public class MaquinasController : Controller
    {

        private DataContextLocal db = new DataContextLocal();

        [Authorize(Roles = "Tecnico")]
        // GET: Maquinas
        public ActionResult Index()
        {
            var maquinas = db.Maquinas.Include(m => m.ModeloMaquina);
            return View(maquinas.ToList());
        }

        [Authorize(Roles = "Tecnico")]
        // GET: Maquinas/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Maquina maquina = db.Maquinas.Find(id);
            if (maquina == null)
            {
                return HttpNotFound();
            }
            return View(maquina);
        }

        [Authorize(Roles = "Admin")]
        // GET: Maquinas/Create
        public ActionResult Create(int? idModeloMaquina)
        {
            var nomeModeloMaquina = db.ModeloMaquinas.FirstOrDefault(mm => mm.IdModeloMaquina == idModeloMaquina);

            ViewBag.IdModeloMaquina = new SelectList(db.ModeloMaquinas, "IdModeloMaquina", "Modelo");
            ViewBag.ModeloMaquina = idModeloMaquina;
            ViewBag.NomeModeloMaquina = nomeModeloMaquina;
            return View();
        }


        // POST: Maquinas/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "IdMaquina,IdModeloMaquina,DinheiroRecebido,Trocos,Descricao,idUser,nomeUser")] Maquina maquina)
        {

            if (ModelState.IsValid)
            {
                //Dados de maquina recem criada
                maquina.DinheiroRecebido = 0;
                maquina.Trocos = 0;
                maquina.nomeUser = "Não atribuido";

                db.Maquinas.Add(maquina);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.IdModeloMaquina = new SelectList(db.ModeloMaquinas, "IdModeloMaquina", "Modelo");
            ViewBag.Error = "Maquina não criada";
            return View(maquina);
        }


        [Authorize(Roles = "Admin")]
        // GET: Maquinas/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Maquina maquina = db.Maquinas.Find(id);
            if (maquina == null)
            {
                return HttpNotFound();
            }

            ViewBag.Users = new SelectList(CombosHelpers.GetUsers(), "", "");
            ViewBag.IdModeloMaquina = new SelectList(db.ModeloMaquinas, "IdModeloMaquina", "Marca", maquina.IdModeloMaquina);
            return View(maquina);
        }

        // POST: Maquinas/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "IdMaquina,IdModeloMaquina,DinheiroRecebido,Trocos,Descricao,idUser,nomeUser")] Maquina maquina)
        {
            if (ModelState.IsValid)
            {
                db.Entry(maquina).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }


            ViewBag.IdModeloMaquina = new SelectList(db.ModeloMaquinas, "IdModeloMaquina", "Marca", maquina.IdModeloMaquina);
            return View(maquina);
        }



        [Authorize(Roles = "Admin")]
        // GET: Maquinas/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Maquina maquina = db.Maquinas.Find(id);
            if (maquina == null)
            {
                return HttpNotFound();
            }
            return View(maquina);
        }

        // POST: Maquinas/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Maquina maquina = db.Maquinas.Find(id);
            db.Maquinas.Remove(maquina);
            db.SaveChanges();
            return RedirectToAction("Index");
        }





        [Authorize(Roles = "Admin")]
        // GET: Maquinas/AtribuirUser/5
        public ActionResult AtribuirUser(int? idMaquina)
        {
            if (idMaquina == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Maquina maquina = db.Maquinas.Find(idMaquina);
            if (maquina == null)
            {
                return HttpNotFound();
            }



           
            ViewBag.IdMaquina = idMaquina;
            return View(maquina);
        }



        // POST: Maquinas/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AtribuirUser(Maquina maquina)
        {
            var maquineta = db.Maquinas.Find(maquina.IdMaquina);

            maquineta.nomeUser = maquina.nomeUser;
            db.Entry(maquineta).State = EntityState.Modified;
            db.SaveChanges();
            return RedirectToAction("ListaMaquinasUser");
        }


        [Authorize(Roles = "Tecnico")]
        // GET: Maquinas
        public ActionResult ListaMaquinasUser()
        {
            var maquinas = db.Maquinas.Include(m => m.ModeloMaquina);
            return View(maquinas.ToList());
        }


        [Authorize(Roles = "Tecnico")]
        // GET: Maquinas/GerirMaquina/5
        public ActionResult GerirMaquina(int? idMaquina)
        {
            if (idMaquina == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            Maquina maquina = db.Maquinas.Find(idMaquina);
            if (maquina == null)
            {
                return HttpNotFound();
            }

            var alertas = db.Alertas.Where(a => a.IdMaquina == idMaquina);

            ViewBag.Alertas = alertas.Count();
            ViewBag.IdMaquina = idMaquina;
            return View(maquina);
        }



        // POST: Maquinas/GerirMaquina/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult GerirMaquina(int idmaquina)
        {

            Maquina maquina = new Maquina();


            //Maquina
            maquina = db.Maquinas.FirstOrDefault(m => m.IdMaquina == idmaquina);

            //Retirar dinheiro recebido da máquina
            maquina.DinheiroRecebido = 0;


            //modelo da maquina
            var modelo = db.ModeloMaquinas.FirstOrDefault(mm => mm.IdModeloMaquina == maquina.IdModeloMaquina);

            //Reposição da quantidade máxima de trocos
            maquina.Trocos = modelo.CapacidadeTrocos;




            if (ModelState.IsValid)
            {

                db.Entry(maquina).State = EntityState.Modified;
                db.SaveChanges();
                return View(maquina);
            }

            ViewBag.IdMaquina = idmaquina;
            ViewBag.Error = "Problema técnico na reposição do dinheiro";
            return View(maquina);
        }




        [Authorize(Roles = "Admin")]
        // GET: Maquinas/ModeloMaquinas
        public ActionResult ModeloMaquinas()
        {
            return View(db.ModeloMaquinas.ToList());
        }




        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
