﻿

namespace ProjFinal.Backend
{
    using Microsoft.AspNet.Identity;
    using Microsoft.AspNet.Identity.EntityFramework;
    using ProjFinal.Backend.Models;
    using System.Data.Entity;
    using System.Web.Mvc;
    using System.Web.Optimization;
    using System.Web.Routing;


    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {

            Database.SetInitializer(
                new MigrateDatabaseToLatestVersion<Models.DataContextLocal,
                Migrations.Configuration>());



            //db para os Logins
            ApplicationDbContext db = new ApplicationDbContext();
            CreateRoles(db);

            //SuperUser
            CreateSuperUser(db);


            AddPermissionsToSuperUser(db);






            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
        }




        //Criação de roles dos users
        private void CreateRoles(ApplicationDbContext db)
        {
            var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(db));


            if (!roleManager.RoleExists("Cliente"))
            {
                roleManager.Create(new IdentityRole("Cliente"));           }

         

            if (!roleManager.RoleExists("Tecnico"))
            {
                roleManager.Create(new IdentityRole("Tecnico"));           }

            

            if (!roleManager.RoleExists("Admin"))
            {
                roleManager.Create(new IdentityRole("Admin"));            }

        }


        //Criar SuperUser na BD
        private void CreateSuperUser(ApplicationDbContext db)
        {
            var userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(db));

            var user = userManager.FindByName("superuser@sapo.pt");

            if (user == null)
            {
                user = new ApplicationUser
                {
                    UserName = "superuser@sapo.pt",
                    Email = "superuser@sapo.pt"
                };

                userManager.Create(user, "superuser123");

            }
        }

        private void AddPermissionsToSuperUser(ApplicationDbContext db)
        {
            var userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(db));
            var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(db));

            var user = userManager.FindByName("superuser@sapo.pt");


            if (!userManager.IsInRole(user.Id, "Cliente"))
            {
                userManager.AddToRole(user.Id, "Cliente");
            }

            if (!userManager.IsInRole(user.Id, "Admin"))
            {
                userManager.AddToRole(user.Id, "Admin");
            }

            if (!userManager.IsInRole(user.Id, "Tecnico"))
            {
                userManager.AddToRole(user.Id, "Tecnico");
            }


        }
    }
}
