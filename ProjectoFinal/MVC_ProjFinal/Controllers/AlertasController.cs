﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using MVC_ProjFinal.Models;

namespace MVC_ProjFinal.Controllers
{
    public class AlertasController : Controller
    {
        private MVC_ProjFinalContext db = new MVC_ProjFinalContext();

        // GET: Alertas
        public ActionResult Index()
        {
            var alertas = db.Alertas.Include(a => a.Maquina);
            return View(alertas.ToList());
        }

        // GET: Alertas/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Alerta alerta = db.Alertas.Find(id);
            if (alerta == null)
            {
                return HttpNotFound();
            }
            return View(alerta);
        }

        // GET: Alertas/Create
        public ActionResult Create()
        {
            ViewBag.IdMaquina = new SelectList(db.Maquinas, "IdMaquina", "Descricao");
            return View();
        }

        // POST: Alertas/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "IdAlerta,IdMaquina,Mensagem")] Alerta alerta)
        {
            if (ModelState.IsValid)
            {
                db.Alertas.Add(alerta);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.IdMaquina = new SelectList(db.Maquinas, "IdMaquina", "Descricao", alerta.IdMaquina);
            return View(alerta);
        }

        // GET: Alertas/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Alerta alerta = db.Alertas.Find(id);
            if (alerta == null)
            {
                return HttpNotFound();
            }
            ViewBag.IdMaquina = new SelectList(db.Maquinas, "IdMaquina", "Descricao", alerta.IdMaquina);
            return View(alerta);
        }

        // POST: Alertas/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "IdAlerta,IdMaquina,Mensagem")] Alerta alerta)
        {
            if (ModelState.IsValid)
            {
                db.Entry(alerta).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.IdMaquina = new SelectList(db.Maquinas, "IdMaquina", "Descricao", alerta.IdMaquina);
            return View(alerta);
        }

        // GET: Alertas/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Alerta alerta = db.Alertas.Find(id);
            if (alerta == null)
            {
                return HttpNotFound();
            }
            return View(alerta);
        }

        // POST: Alertas/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Alerta alerta = db.Alertas.Find(id);
            db.Alertas.Remove(alerta);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
