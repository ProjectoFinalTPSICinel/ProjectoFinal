namespace MVC_ProjFinal.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<MVC_ProjFinal.Models.MVC_ProjFinalContext>
    {
        public Configuration()
        {
           
            AutomaticMigrationsEnabled = true;
            AutomaticMigrationDataLossAllowed = true;
            ContextKey = "MVC_ProjFinal.Models.MVC_ProjFinalContext";
        }

        protected override void Seed(MVC_ProjFinal.Models.MVC_ProjFinalContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data.
        }
    }
}
