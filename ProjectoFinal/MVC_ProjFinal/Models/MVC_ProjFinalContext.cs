﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace MVC_ProjFinal.Models   
{

    using BibliotecaClasses;

    public class MVC_ProjFinalContext : DataContext
    {
        // You can add custom code to this file. Changes will not be overwritten.
        // 
        // If you want Entity Framework to drop and regenerate your database
        // automatically whenever you change your model schema, please use data migrations.
        // For more information refer to the documentation:
        // http://msdn.microsoft.com/en-us/data/jj591621.aspx

        //public MVC_ProjFinalContext() : base("name=MVC_ProjFinalContext")
        //{
        //}

        public DbSet<Maquina> Maquinas { get; set; }

        public DbSet<Produto> Produtoes { get; set; }

        public DbSet<ModeloMaquina> ModeloMaquinas { get; set; }

        public DbSet<ProdutoMaquina> ProdutoMaquinas { get; set; }

        public DbSet<Alerta> Alertas { get; set; }
    }
}
