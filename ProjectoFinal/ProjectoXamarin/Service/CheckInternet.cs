﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Text;

namespace ProjectoXamarin.Service
{
    public class CheckInternet
    {
        public static bool Verify()
        {
            string CheckUrl = "http://google.com";

            try
            {
                HttpWebRequest iNetRequest = (HttpWebRequest)WebRequest.Create(CheckUrl);

                iNetRequest.Timeout = 5000;

                WebResponse iNetResponse = iNetRequest.GetResponse();

                iNetResponse.Close();

                return true;

            }
            catch (WebException)
            {
                return false;
            }
        }
    }
}
