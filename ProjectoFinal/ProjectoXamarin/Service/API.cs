﻿namespace ProjectoXamarin.Service
{
    using Newtonsoft.Json;
    using Newtonsoft.Json.Linq;
    using ProjectoXamarin.Models;
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.IO;
    using System.Net;
    using System.Net.Http;
    using System.Net.Http.Headers;
    using System.Text;
    using System.Linq;
    using Newtonsoft.Json.Schema;
    using System.Threading.Tasks;



    public class API
    {
        static HttpWebRequest WebReq;
        static string urlbase = "http://projfinalapi1.azurewebsites.net/";




        public static async Task<TokenResponse> GetToken(string userName, string password)
        {
            try
            {
                var cliente = new HttpClient();
                cliente.BaseAddress = new Uri(urlbase);
                var response = await cliente.PostAsync("Token",
                    new StringContent(
                        string.Format("grant_type=password&userName={0}&password={1}", userName, password),
                        Encoding.UTF8,
                        "application/x-www-form-urlencoded"
                        )
                 );

                var resultJson = await response.Content.ReadAsStringAsync();

                var result = JsonConvert.DeserializeObject<TokenResponse>(resultJson);

                return result;
            }
            catch (Exception)
            {
                return null;
            }
        }










        public static async Task<int> VerificarToken(string User, string Senha)
        {


            var token = await GetToken(User, Senha);
            if (token == null)
            {
                return 0;
            }
            if (string.IsNullOrEmpty(token.AccessToken))
            {
                return 0;
            }
            return 1;
        }


        // Login
        /*
             1 - Sucesso
             2 - Senha ou user Incorrecto
             3 - Conta não existe
             4 - Maquina não atribuida
             5 - Sem acesso à internet
         */
        public static  int CheckLoginAsync(string User, string Senha)
        {
            if (!CheckInternet.Verify())
            {
                return 5;
            }
            if (Task.Factory.StartNew(() => VerificarToken(User, Senha).Result).Result == 0)
            {
                return 2;
            }
            else
            {
                return 1;
            }

           
        }

        public static async Task<string> RegistarLoginAsync(string User, string Password, string ConfirmPassword)
        {
            
            if(String.IsNullOrWhiteSpace(User) || string.Empty == User)
            {
                return "Preencha o campo Login";
            }
            else if (String.Empty == Password || String.IsNullOrWhiteSpace(Password))
            {
                return "Preencha o campo Senha";
            }
            else if (String.Empty == ConfirmPassword || String.IsNullOrWhiteSpace(ConfirmPassword))
            {
                return "Preencha o campo Confirmar Password";
            }
            else
            {
                if(ConfirmPassword != Password)
                {
                    return "O campo Confirmar Password é diferente do campo Password";
                }else
                {

                    List<char> array =  Password.ToList();
                    bool Letra = false;
                    bool Simbolo = false;
                    bool Numero = false;
                    bool UpperCase = false;
                    bool Lower = false;
                    foreach (var caracter in array)
                    {
                       if( char.IsLetter(caracter) == true)
                        {
                            if(char.IsUpper(caracter))
                            {
                                UpperCase = true;
                            }
                            if(char.IsLower(caracter))
                            {
                                Lower = true;
                            }
                            Letra = true;
                        }
                       if(!char.IsLetterOrDigit(caracter))
                        {
                            Simbolo = true;
                        }
                       
                            
                
                       if(char.IsNumber(caracter) == true)
                        {
                            Numero = true;
                        }

                      

                    }

                          if (!Letra)
                          {
                              return "A Password tem de conter pelo menos uma letra";
                          }
                          if (!Simbolo)
                          { 

                              return "A Password tem de conter pelo menos um simbolo";
                          }
                       
                          if(array.Count < 6)
                          {
                             return "A Password tem de conter pelo menos 6 caracteres";
                          }
                          if(!Numero)
                          {
                               return "A Password tem de conter pelo menos 1 numero";
                          }
                          if(!UpperCase)
                         {
                            return "A Password tem de conter pelo menos 1 maiúscula ";
                         }
                          if(!Lower)
                          {
                             return "A Password tem de conter pelo menos 1 minuscula ";
                         }
                          if(!IsValidEmail(User))
                           {
                                return "O Utilizador não é um email ";
                           }
                           

                         var client = new HttpClient();
                        client.BaseAddress = new Uri(urlbase);

                        string jsonData = "{\"Email\":\"" + User + "\"," + "\"Password\":\"" + Password + "\"," + "\"ConfirmPassword\":\"" + ConfirmPassword + "\"}";

                        var content = new StringContent(jsonData, Encoding.UTF8, "application/json");
                        HttpResponseMessage response = await client.PostAsync("api/Account/Register", content);
                       ErrorRegisto result = null;
                        try
                        {
                           result = JsonConvert.DeserializeObject<ErrorRegisto>(response.Content.ReadAsStringAsync().Result);
                            if ("The request is invalid." == result.Message.ToString())
                            {
                                return "O e-mail introduzido já existe";
                            }
                        }
                        catch (Exception)
                        {

                           
                        }

                       

                    return "Registado com sucesso!";
                }
            }
        }

        private static bool IsValidEmail(string email)
        {
            try
            {
                var addr = new System.Net.Mail.MailAddress(email);
                return addr.Address == email;
            }
            catch
            {
                return false;
            }
        }



        public static object GetProdutos(string user)
        {
            try
            {
                WebReq = (HttpWebRequest)WebRequest.Create(string.Format(urlbase + "api/Produtos/GetByUser/" + user + "/"));
            }
            catch (Exception e)
            {

                Debug.WriteLine(e.Message);
            }
           
            WebReq.Method = "GET";

            HttpWebResponse WebResp = (HttpWebResponse)WebReq.GetResponse();

            string jsonString;
            using (Stream stream = WebResp.GetResponseStream())   
            {
                StreamReader reader = new StreamReader(stream, Encoding.UTF8);
                jsonString = reader.ReadToEnd();
            }

            object retorno = null;
            try
            {
                retorno = JsonConvert.DeserializeObject<Error>(jsonString);
            }
            catch (Exception)
            {

                try
                {
                    retorno = JsonConvert.DeserializeObject<List<Produto>>(jsonString);
                }
                catch (Exception)
                {

                    retorno = null;
                }

            }
            return retorno;
        }
     
    }

}
