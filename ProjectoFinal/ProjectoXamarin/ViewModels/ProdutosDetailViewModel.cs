﻿using GalaSoft.MvvmLight.Command;
using ProjectoXamarin.Models;
using ProjectoXamarin.Service;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Input;
using Xamarin.Forms;

namespace ProjectoXamarin.ViewModels
{
    public class ProdutosDetailViewModel : BaseViewModel
    {
        private string user;
        private static List<Produto> produtos = new List<Produto>();
     
        private ObservableCollection<Produto> _items = new ObservableCollection<Produto>(produtos);

        private string _SearchText;

         public string SearchText
        {
            get { return _SearchText; }

            set
            {
                SetValue(ref this._SearchText, value);
            }
        }

       

        public ICommand PesquisarCommand
        {
            get
            {
                return new RelayCommand(Pesquisar);
            }
        }

        public void Pesquisar()
        {
            
            if (string.IsNullOrEmpty(_SearchText) || String.Empty == _SearchText)
            {
                _items.Clear();
                foreach (var item in produtos)
                {
                    _items.Add(item);

                }
                
            }
            else
            {
                _items.Clear();
                var resultados = produtos.Where(x => (x.Nome).ToUpper().StartsWith(_SearchText.ToUpper())).ToList();
                foreach (var item in resultados)
                {
                    _items.Add(item);

                }
            }
        }

       

        public ObservableCollection<Produto> Items
        {
            get { return _items; }

            set
            {
                SetValue(ref this._items, value);
            }
        }


        public void ApagarLista()
        {
            _items.Clear();
            produtos.Clear();
            CarregarLista(user);
        }
        public ProdutosDetailViewModel(string User)
        {
            user = User.Trim();
            ApagarLista();
        }
        public void CarregarLista(string User)
        {
            try
            {
                var api = API.GetProdutos(user);
                if (api is Error error)
                {
                    
                    Application.Current.MainPage.DisplayAlert("Error", error.ErrorMessage, "OK");
                }
               if(api is List<Produto>)
                {
                    var produtoalterados = (List<Produto>)api;
                    foreach (var item in produtoalterados)
                    {
                        string[] words = item.Imagem.Split('/');
                        
                        var p =  new Produto();
                        p.Preco = item.Preco;
                        p.Nome = item.Nome;
                        p.Descricao = item.Descricao;
                        p.Imagem = "http://projfinalbackendcet30.azurewebsites.net/Content/Imagens/" + words[3];
                        produtos.Add(p);
                    }
                    



                }
                
            }
            catch (Exception e)
            {
                _items.Clear();
                Application.Current.MainPage.DisplayAlert("Error", "Ocorreu um erro com a ligação\n" + e.Message, "OK");

               
            }
             _items = new ObservableCollection<Produto>(produtos);
        }

        
       
    }

}
