﻿namespace ProjectoXamarin.ViewModels
{
    using GalaSoft.MvvmLight.Command;
    using ProjectoXamarin.Service;
    using ProjectoXamarin.Views;
    using System.Windows.Input;
    using Xamarin.Forms;

    public class LoginViewModel : BaseViewModel
    {
       
       #region Events

       // public event PropertyChangedEventHandler PropertyChanged;

       #endregion

       #region Atributes

       private string _user;
       private string _senha;


       #endregion

       #region Properties

      public string Senha
       {
           get { return _senha; }

           set
           {
               SetValue(ref this._senha, value);
           }
       }


        public string User
        {
            get { return _user; }

            set
            {
                SetValue(ref this._user, value);
            }
        }



        #endregion

        #region Commands

        public ICommand LoginCommand
        {
            get
            {
                return new RelayCommand(Login);
            }
         }





        private void Login()
        {
            
            if (string.IsNullOrEmpty(User))
            {
                Application.Current.MainPage.DisplayAlert("Error", "Preencha o campo Utilizador", "OK");
                return;
            }

            if (string.IsNullOrEmpty(Senha))
            {
                Application.Current.MainPage.DisplayAlert("Error", "Preencha o campo Senha", "OK");
                return;
            }
            switch (API.CheckLoginAsync(User, Senha))
            {
                case 1:
                    {
                        MainViewModel.GetInstance().ProdutosMaster = new ProdutosMasterViewModels();
                        MainViewModel.GetInstance().ProdutosDetails = new ProdutosDetailViewModel(User);
                        Application.Current.MainPage.Navigation.PushAsync(new Produtos());

                        break;
                    }

                case 2: Application.Current.MainPage.DisplayAlert("Error", "Senha ou Utilizador incorrecto", "OK"); break;
                case 5: Application.Current.MainPage.DisplayAlert("Error", "Não foi possivel estabelecer conecção à internet", "OK"); break;
            }
         

        }






        public ICommand RegistarCommand
        {
            get
            {
                return new RelayCommand(RegistarUser);
            }
        }
        #endregion

        private void RegistarUser()
        {
            MainViewModel.GetInstance().Registar = new RegistarViewModel();
         
            Application.Current.MainPage.Navigation.PushAsync(new RegistarPage());
        }









            #region Constructores

            public LoginViewModel()
            {

           
            }

   #endregion

    }
}
