﻿namespace ProjectoXamarin.ViewModels
{
    using GalaSoft.MvvmLight.Command;
    using ProjectoXamarin.Models;
    using System.Windows.Input;
    using Xamarin.Forms;

    public class ProdutoViewModel : BaseViewModel
    {

        private string _Nome;
        private string _Detalhes;
        private string _Imagem;
        private double _Preco;
      
        public ProdutoViewModel(Produto produto)
        {
            _Nome = produto.Nome;
            _Detalhes = produto.Descricao;
            _Preco = produto.Preco;
            _Imagem = produto.Imagem;


        }
        public double Preco
        {
            get { return _Preco; }

            set
            {
                SetValue(ref this._Preco, value);
            }
        }
        

        public string Imagem
        {
            get { return _Imagem; }

            set
            {
                SetValue(ref this._Imagem, value);
            }
        }



        public string Detalhes
        {
            get { return _Detalhes; }

            set
            {
                SetValue(ref this._Detalhes, value);
            }
        }

        public string Nome
        {
            get { return _Nome; }

            set
            {
                SetValue(ref this._Nome, value);
            }
        }

        public ICommand CancelarCommand
        {
            get
            {
                return new RelayCommand(Cancelar);
            }
        }

        public ICommand ComprarCommand
        {
            get
            {
                return new RelayCommand(Cancelar);
            }

        }

        private void Cancelar()
        {

            Application.Current.MainPage.Navigation.PopAsync();
        }
    }
}
