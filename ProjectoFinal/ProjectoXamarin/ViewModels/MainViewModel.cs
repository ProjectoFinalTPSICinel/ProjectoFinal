﻿namespace ProjectoXamarin.ViewModels
{
    public class MainViewModel
    {

        #region ViewModels

        public LoginViewModel Login { get; set; }

        public RegistarViewModel Registar { get; set; }

        public ProdutosDetailViewModel ProdutosDetails { get; set; }

        public ProdutosMasterViewModels ProdutosMaster { get; set; }

        public ProdutoViewModel ProdutoVModel { get; set; }

        #endregion

        #region Properties
        #endregion

        #region Constructores

        public MainViewModel()
        {
            instance = this;
            this.Login = new LoginViewModel();
           
        }

        #endregion

        #region Singleton

        private static MainViewModel instance;




        public static MainViewModel GetInstance()
        {
            if (instance == null)
            {
                return new MainViewModel();
            }
            else
            {
                return instance;
            }

        }
        #endregion
    }
}
