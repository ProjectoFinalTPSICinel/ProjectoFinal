﻿

namespace ProjectoXamarin.ViewModels
{
    using GalaSoft.MvvmLight.Command;
    using ProjectoXamarin.Service;
    using ProjectoXamarin.Views;
    using System.Windows.Input;
    using Xamarin.Forms;


    public class RegistarViewModel : BaseViewModel
    {

        public ICommand CancelarCommand
        {
            get
            {
                return new RelayCommand(Cancelar);
            }
        }
        //RepetirSenha Senhas

        private string _user;
        private string _senha;
        private string _repetirSenha;
        private string _error;

        public string Error
        {
            get
            {
                return _error;
            }
            set
            {
                SetValue(ref this._error, value);
            }
        }



        public string User
        {
            get { return _user; }

            set
            {
                SetValue(ref this._user, value);
            }
        }

        public string Senha
        {
            get { return _senha; }

            set
            {
                SetValue(ref this._senha, value);
            }
        }


        public string RepetirSenha
        {
            get { return _repetirSenha; }

            set
            {
                SetValue(ref this._repetirSenha, value);
            }
        }

        public ICommand RegistarCommand
        {
            get
            {
                return new RelayCommand(Registar);
            }
        }
        private async void Registar()
        {
           string msg = await API.RegistarLoginAsync(User, Senha, RepetirSenha);
           Error = msg;
           await Application.Current.MainPage.DisplayAlert("Registar", msg, "ok");
            if(msg == "Registado com sucesso!")
            {
                await Application.Current.MainPage.Navigation.PopToRootAsync();
            }
        }


        private void Cancelar()
        {
            Application.Current.MainPage.Navigation.PushAsync(new LoginPage());
        }
    }
}