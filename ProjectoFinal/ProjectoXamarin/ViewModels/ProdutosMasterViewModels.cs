﻿

namespace ProjectoXamarin.ViewModels
{
 
    using GalaSoft.MvvmLight.Command;
    using ProjectoXamarin.Views;
    using System.Collections.ObjectModel;
    using System.Windows.Input;
    using Xamarin.Forms;

    public class ProdutosMasterViewModels : BaseViewModel
    {

        public ICommand LoginCommand
        {
            get
            {
                return new RelayCommand(Login);
            }
        }

        public ObservableCollection<ProdutosMenuItem> MenuItems { get; set; }



        public void Login()
        {
            Application.Current.MainPage.Navigation.PushAsync(new LoginPage());
        }

        public ProdutosMasterViewModels()
        {
            MenuItems = new ObservableCollection<ProdutosMenuItem>(new[]
           {
                    new ProdutosMenuItem { Id = 0, Title = "Produtos" },

            });
        }

    }
}
