﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProjectoXamarin.Models
{
    public class Error
    {
        public string ErrorMessage { get; set; }
    }

    public class ModelState
    {
        public List<string> __invalid_name__ { get; set; }
    }

    public class ErrorRegisto
    {
        public string Message { get; set; }
        public ModelState ModelState { get; set; }
    }
}
