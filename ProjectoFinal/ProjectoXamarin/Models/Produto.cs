﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProjectoXamarin.Models
{
    public class Produto
    {
        public int IdProduto { get; set; }
        public string Nome { get; set; }
        public string Descricao { get; set; }
        public double Preco { get; set; } 
        public string Imagem { get; set; }
        public int CategoriaProduto { get; set; }

    }
}
