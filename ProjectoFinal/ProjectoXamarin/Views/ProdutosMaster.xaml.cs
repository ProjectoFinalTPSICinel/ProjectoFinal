﻿using ProjectoXamarin.ViewModels;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ProjectoXamarin.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ProdutosMaster : ContentPage
    {
        public ListView ListView;

        public ProdutosMaster()
        {
            InitializeComponent();
            ListView = MenuItemsListView;
            
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            //MainViewModel.GetInstance().ProdutosDetails.ApagarLista();
            ListView = MenuItemsListView;

        }
    

}
}