﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectoXamarin.Views
{

    public class ProdutosMenuItem
    {
        public ProdutosMenuItem()
        {
            TargetType = typeof(ProdutosDetail);
        }
        public int Id { get; set; }
        public string Title { get; set; }

        public Type TargetType { get; set; }
    }
}