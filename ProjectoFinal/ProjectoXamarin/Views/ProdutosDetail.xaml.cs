﻿
using ProjectoXamarin.Models;
using ProjectoXamarin.ViewModels;
using System.Collections.ObjectModel;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ProjectoXamarin.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ProdutosDetail : ContentPage
    {
       
        public ObservableCollection<Models.Produto> Items { get; set; }

        public ProdutosDetail()
        {
            InitializeComponent();
            Device.BeginInvokeOnMainThread(() => {
                Title = "Produtos";
            });
           
           
        }

        async void Handle_ItemTapped(object sender, ItemTappedEventArgs e)
        {
            if (e.Item == null)
                return;
            
            MainViewModel.GetInstance().ProdutoVModel = new ProdutoViewModel((Produto)e.Item);
            await Application.Current.MainPage.Navigation.PushAsync(new ProdutoPage());

            ((ListView)sender).SelectedItem = null;
        }
    }
}
