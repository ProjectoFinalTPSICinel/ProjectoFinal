﻿

namespace ProjFinal.Domain
{

    using System.Data.Entity;

    public class DataContext:DbContext
    {
        public DataContext():base("DefaultConnection")
        {

        }


        public System.Data.Entity.DbSet<ProjFinal.Domain.ModeloMaquina> ModeloMaquinas { get; set; }

        public System.Data.Entity.DbSet<ProjFinal.Domain.Maquina> Maquinas { get; set; }

        public System.Data.Entity.DbSet<ProjFinal.Domain.Produto> Produtoes { get; set; }

        public System.Data.Entity.DbSet<ProjFinal.Domain.ProdutoMaquina> ProdutoMaquinas { get; set; }

        public System.Data.Entity.DbSet<ProjFinal.Domain.Alerta> Alertas { get; set; }

        public System.Data.Entity.DbSet<ProjFinal.Domain.Fatura> Faturas { get; set; }

        public System.Data.Entity.DbSet<ProjFinal.Domain.Stock> Stocks { get; set; }
    }
}
