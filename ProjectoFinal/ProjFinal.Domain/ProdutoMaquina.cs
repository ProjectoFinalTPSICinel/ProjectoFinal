﻿

namespace ProjFinal.Domain
{
    using Newtonsoft.Json;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    public class ProdutoMaquina
    {

        [Key]
        [Display(Name = "Produto na Maquina")]
        public int IdProdutoMaquina { get; set; }

        [Display(Name = "Maquina nº")]
        public int IdMaquina { get; set; }

        [Display(Name = "Produto")]
        public int IdProduto { get; set; }             

        
        [Display(Name = "Validade do lote do produto na máquina")]
        [DataType(DataType.Date)]
        public DateTime Validade { get; set; }


        [JsonIgnore]
        public virtual ICollection<Fatura> Faturas { get; set; }

        public virtual Produto Produto { get; set; }

        public virtual Maquina Maquina { get; set; }



    }
}
