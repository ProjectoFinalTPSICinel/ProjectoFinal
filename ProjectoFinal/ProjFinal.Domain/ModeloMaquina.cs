﻿

namespace ProjFinal.Domain
{
    using Newtonsoft.Json;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    

    public class ModeloMaquina
    {

        [Key]
        public int IdModeloMaquina { get; set; }
        [Required]
        public string Marca { get; set; }
        [Required]
        public string Modelo { get; set; }

        [Range(0,500 , ErrorMessage = "Deve inserir um valor maior do que 0!!")]
        public int CapacidadeProdutos { get; set; }
        [Range(0, 500, ErrorMessage = "Deve inserir um valor maior do que 0!!")]
        public int CapacidadeDinheiro { get; set; }
        [Range(0, 500, ErrorMessage = "Deve inserir um valor maior do que 0!!")]
        public int CapacidadeTrocos { get; set; }
        [Required]
        public TipoMaquina TipoMaquina { get; set; }


        [JsonIgnore]
        public virtual ICollection<Maquina> Maquina  { get; set; }



    }
}
