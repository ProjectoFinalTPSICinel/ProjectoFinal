﻿

namespace ProjFinal.Domain
{
    using Newtonsoft.Json;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    public class Produto
    {
        [Key]
        public int IdProduto { get; set; }

        [Display(Name = "Nome do Produto")]
        public string Nome { get; set; }


        [DataType(DataType.MultilineText)]
        [Display(Name = "Descrição do Produto")]
        public string Descricao { get; set; }

        [DisplayFormat(DataFormatString = "{0:C2}", ApplyFormatInEditMode = false)]
        [DataType(DataType.Currency)]
        [Display(Name = "Preço do Produto")]
        public double Preco { get; set; }

        [Display(Name = "Categoria de Produto")]
        public CategoriaProduto CategoriaProduto { get; set; }
        
       
        public string Imagem { get; set; }

        [JsonIgnore]
        public virtual ICollection<ProdutoMaquina> ProdutoMaquina { get; set; }

        [JsonIgnore]
        public virtual ICollection<Stock> StocksDasMaquinas { get; set; }

    }
}
