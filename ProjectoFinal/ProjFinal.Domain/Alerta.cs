﻿
namespace ProjFinal.Domain
{
    using Newtonsoft.Json;
    using System;
    using System.ComponentModel.DataAnnotations;

    public class Alerta
    {
        [Key]
        public int IdAlerta { get; set; }

        public int IdMaquina { get; set; }

        public string Mensagem { get; set; }

        public StatusAlerta Status { get; set; }

        [Display(Name ="Data e hora do Aviso")]
        public DateTime Data { get; set; }
        
        [JsonIgnore]
        public virtual Maquina Maquina { get; set; }

    }
}
