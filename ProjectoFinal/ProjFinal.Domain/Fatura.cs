﻿

namespace ProjFinal.Domain
{
    using Newtonsoft.Json;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    public class Fatura
    {

        [Display(Name = "Nº de Venda")]
        [Key]
        public int IdFatura { get; set; }

        [Display(Name = "Produto Vendido")]
        public int IdProdutoMaquina { get; set; }

        [Display(Name = "Valor da Venda")]
        public double Valor { get; set; }

        [DataType(DataType.Date)]
        [Display(Name = "Data da Venda")]
        public DateTime Data { get; set; }


        
        [JsonIgnore]
        public virtual ProdutoMaquina ProdutoMaquina{ get; set; }



    }
}
