﻿

namespace ProjFinal.Domain
{
    public enum CategoriaProduto
    {
        Bebidas,
        Sobremesas,
        Sandes,
        Sopas,
        Pastelaria,
        Chocolates,
        Cafe,
        Outros

    }
}
