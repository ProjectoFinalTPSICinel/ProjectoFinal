﻿
namespace ProjFinal.Domain
{
    using Newtonsoft.Json;
    using System.Collections;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;


    //Configuração do stock de produtos de cada máquina
    //permite saber quais os produtos deveriam estar sempre na máquina
    //permite também os alertas de falta de stock
    public class Stock
    {
        [Key]
        public int IdStock { get; set; }

        [Display(Name = "Maquina")]
        public int IdMaquina { get; set; }

        [Display(Name ="Produto para vender na Maquina")]
        public int IdProduto { get; set; }

        [Display(Name = "Stock de Segurança")]
        public int StockSeguranca { get; set; }

        [JsonIgnore]
        public virtual Maquina Maquina { get; set; }

        [JsonIgnore]
        public virtual Produto Produto { get; set; }


        
    }
}
