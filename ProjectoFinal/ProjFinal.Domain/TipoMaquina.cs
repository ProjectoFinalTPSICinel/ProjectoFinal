﻿
namespace ProjFinal.Domain
{
    using System.ComponentModel.DataAnnotations;

    public enum TipoMaquina
    {

        [Display(Name = "Máquina de Cafés")]
        Cafe,
        
        [Display(Name = "Máquina de Alimentos e Bebidas")]
        AlimentosBebidas


    }
}
